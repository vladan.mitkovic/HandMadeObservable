package me.mitkovic.android.handmadeobservable.common;

import android.app.Activity;
import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.HasActivityInjector;
import me.mitkovic.android.handmadeobservable.common.di.DaggerApplicationComponent;

public class HandMadeObservableApplication extends Application implements HasActivityInjector {

    @Inject
    AndroidInjector<Activity> activityInjector;

    @Override
    public void onCreate() {
        super.onCreate();

        setup();
    }

    protected void setup() {
        DaggerApplicationComponent
                .builder()
                .application(this)
                .build()
                .inject(this);

        Fresco.initialize(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityInjector;
    }

}
