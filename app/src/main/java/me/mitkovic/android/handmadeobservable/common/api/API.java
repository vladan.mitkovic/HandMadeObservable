package me.mitkovic.android.handmadeobservable.common.api;

import com.google.gson.Gson;

import java.io.IOException;
import java.lang.reflect.Type;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import me.mitkovic.android.handmadeobservable.common.api.exception.ServerException;
import me.mitkovic.android.handmadeobservable.common.api.model.ServerResponse;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public abstract class API {

    public <T> Observable<T> post(final OkHttpClient client, final Gson gson, String url, final Type responseType) {
        final Request request = new Request.Builder()
                .url(url)
                .build();

        return observable(client, gson, request, responseType);
    }

    private <T> Observable<T> observable(final OkHttpClient client, final Gson gson, final Request request, final Type responseType) {
        return Observable.create(new ObservableOnSubscribe<T>() {
            @Override
            public void subscribe(final ObservableEmitter<T> emitter) throws Exception {

                final Call call = client.newCall(request);
                call.enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            emitter.onError(e);
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            if (emitter.isDisposed()) return;

                            ServerResponse<T> parsedResponse;
                            try {
                                parsedResponse = gson.fromJson(response.body().charStream(), responseType);
                            } catch (Exception e) {
                                emitter.onError(e);
                                return;
                            }

                            if (response.code() == 200) {
                                emitter.onNext(parsedResponse.getData());
                                emitter.onComplete();
                            } else {
                                emitter.onError(new ServerException(parsedResponse.getErrors()));
                            }
                        }
                    });
            }
        });
    }

}
