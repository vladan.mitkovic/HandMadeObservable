package me.mitkovic.android.handmadeobservable.common.api;


import java.util.List;

import io.reactivex.Observable;
import me.mitkovic.android.handmadeobservable.common.api.model.User;

public interface UsersAPI {

    Observable<List<User>> getUsers();

}
