package me.mitkovic.android.handmadeobservable.common.api;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import hugo.weaving.DebugLog;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import me.mitkovic.android.handmadeobservable.common.api.model.ServerResponse;
import me.mitkovic.android.handmadeobservable.common.api.model.User;
import me.mitkovic.android.handmadeobservable.common.api.model.UsersResponse;
import okhttp3.OkHttpClient;

public class UsersAPIImpl extends API implements UsersAPI {

    private static final Type USERS_TYPE = new TypeToken<ServerResponse<UsersResponse>>() {
    }.getType();

    private final OkHttpClient client;
    private final Gson gson;
    private final String url;

    public UsersAPIImpl(String url, OkHttpClient client, Gson gson) {
        this.client = client;
        this.gson = gson;
        this.url = url;
    }

    @Override
    public Observable<List<User>> getUsers() {

        Observable<UsersResponse> response = post(client, gson, url + "getusers", USERS_TYPE);
        return response
                .map(userResponseToUsersList());
    }

    private Function<UsersResponse, List<User>> userResponseToUsersList() {
        return new Function<UsersResponse, List<User>>() {

            @Override
            @DebugLog
            public List<User> apply(UsersResponse usersResponse) throws Exception {
                return usersResponse.getUsers();
            }
        };
    }

}
