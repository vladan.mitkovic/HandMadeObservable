package me.mitkovic.android.handmadeobservable.common.api.exception;

import java.util.List;

import me.mitkovic.android.handmadeobservable.common.api.model.APIError;

public class ServerException extends Exception {

    private List<APIError> apiErrors;

    public ServerException(List<APIError> apiErrors) {
        this.apiErrors = apiErrors;
    }

    public List<APIError> getApiErrors() {
        return apiErrors;
    }
}
