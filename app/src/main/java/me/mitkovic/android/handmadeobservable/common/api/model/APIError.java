package me.mitkovic.android.handmadeobservable.common.api.model;

public class APIError {

    private String message;
    private String code;
    private String statusCode;
    private Long properValue;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getProperValue() {
        return properValue;
    }

    public void setProperValue(Long properValue) {
        this.properValue = properValue;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "APIError { " +
                "message='" + message + '\'' +
                ", code='" + code + '\'' +
                ", statusCode='" + statusCode + '\'' +
                ", properValue=" + properValue +
                '}';
    }
}
