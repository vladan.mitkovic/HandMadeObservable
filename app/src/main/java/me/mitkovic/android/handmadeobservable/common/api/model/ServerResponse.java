package me.mitkovic.android.handmadeobservable.common.api.model;

import java.util.List;

public class ServerResponse<T> {

    private T data;
    private List<APIError> errors;

    public ServerResponse(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<APIError> getErrors() {
        return errors;
    }

    public void setErrors(List<APIError> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "ServerResponse{" +
                "data=" + data +
                ", errors=" + errors +
                '}';
    }
}
