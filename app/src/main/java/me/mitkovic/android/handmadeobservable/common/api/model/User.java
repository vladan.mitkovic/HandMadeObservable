package me.mitkovic.android.handmadeobservable.common.api.model;

public class User {

    private int id;
    private String first_name;
    private String profile_picture_small;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return first_name;
    }

    public String getProfilePictureSmall() {
        return profile_picture_small;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", profile_picture_small='" + profile_picture_small + '\'' +
                '}';
    }
}
