package me.mitkovic.android.handmadeobservable.common.api.model;

import java.util.List;

public class UsersResponse {

    private List<User> items;

    private UsersResponse(List<User> users) {
        this.items = users;
    }

    public List<User> getUsers() {
        return items;
    }

    @Override
    public String toString() {
        return "UsersResponse{" +
                "items=" + items +
                '}';
    }
}
