package me.mitkovic.android.handmadeobservable.common.di;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import me.mitkovic.android.handmadeobservable.common.HandMadeObservableApplication;
import me.mitkovic.android.handmadeobservable.common.di.module.APIModule;
import me.mitkovic.android.handmadeobservable.common.di.module.AppModule;
import me.mitkovic.android.handmadeobservable.common.di.module.ApplicationModule;
import me.mitkovic.android.handmadeobservable.common.di.scope.HandMadeObservableApplicatonScope;


@HandMadeObservableApplicatonScope
@Component(modules = {
        AndroidInjectionModule.class,
        ApplicationModule.class,
        APIModule.class,
        AppModule.class
})
public interface ApplicationComponent {

    void inject(HandMadeObservableApplication notifyMeApplication);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(HandMadeObservableApplication application);
        ApplicationComponent build();
    }

}
