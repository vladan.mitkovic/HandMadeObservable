package me.mitkovic.android.handmadeobservable.common.di.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import me.mitkovic.android.handmadeobservable.BuildConfig;
import me.mitkovic.android.handmadeobservable.common.api.UsersAPI;
import me.mitkovic.android.handmadeobservable.common.api.UsersAPIImpl;
import me.mitkovic.android.handmadeobservable.common.di.scope.HandMadeObservableApplicatonScope;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

@Module
public class APIModule {

    @Provides
    @HandMadeObservableApplicatonScope
    public OkHttpClient providesOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        HttpLoggingInterceptor headerLogging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        HttpLoggingInterceptor bodyLogging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(headerLogging)
                .addInterceptor(bodyLogging)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @HandMadeObservableApplicatonScope
    public Gson providesGson(GsonBuilder gsonBuilder) {
        return gsonBuilder.create();
    }

    @Provides
    @HandMadeObservableApplicatonScope
    public GsonBuilder providesGsonBuilder() {
        return new GsonBuilder();
    }

    @Provides
    @HandMadeObservableApplicatonScope
    public String providesAPIURL() {
        return BuildConfig.API_ENDPOINT;
    }

    @Provides
    @HandMadeObservableApplicatonScope
    public static UsersAPI providesUsersAPI(String apiURL, Gson gson, OkHttpClient client) {
        return new UsersAPIImpl(apiURL, client, gson);
    }

}
