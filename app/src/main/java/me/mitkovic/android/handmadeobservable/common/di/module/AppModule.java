package me.mitkovic.android.handmadeobservable.common.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import me.mitkovic.android.handmadeobservable.common.di.scope.PerActivity;
import me.mitkovic.android.handmadeobservable.home.di.MainModule;
import me.mitkovic.android.handmadeobservable.home.view.MainActivity;

@Module
public abstract class AppModule {

    @PerActivity
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity usersActivityInjector();
}
