package me.mitkovic.android.handmadeobservable.common.di.module;

import android.app.Activity;
import android.content.Context;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import me.mitkovic.android.handmadeobservable.common.HandMadeObservableApplication;
import me.mitkovic.android.handmadeobservable.common.di.scope.ApplicationContext;
import me.mitkovic.android.handmadeobservable.common.di.scope.HandMadeObservableApplicatonScope;

@Module
public abstract class ApplicationModule {

    @HandMadeObservableApplicatonScope
    @Binds
    abstract AndroidInjector<Activity> bindsActivityInjector(DispatchingAndroidInjector<Activity> injector);

    @Provides
    @HandMadeObservableApplicatonScope
    @ApplicationContext
    public static Context providesContext(HandMadeObservableApplication application) {
        return application.getApplicationContext();
    }

}
