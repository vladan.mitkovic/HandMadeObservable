package me.mitkovic.android.handmadeobservable.common.di.scope;

import javax.inject.Qualifier;

@Qualifier
public @interface ApplicationContext {
}
