package me.mitkovic.android.handmadeobservable.home.di;

import dagger.Binds;
import dagger.Module;
import me.mitkovic.android.handmadeobservable.common.di.scope.PerActivity;
import me.mitkovic.android.handmadeobservable.home.view.MainPresenter;
import me.mitkovic.android.handmadeobservable.home.view.MainPresenterImpl;

@PerActivity
@Module
public abstract class MainModule {

    @Binds
    abstract MainPresenter bindsMainPresenter(MainPresenterImpl presenter);

}
