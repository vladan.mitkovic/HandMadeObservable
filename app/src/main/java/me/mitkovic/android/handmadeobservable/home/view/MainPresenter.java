package me.mitkovic.android.handmadeobservable.home.view;


import android.content.Context;

import me.mitkovic.android.handmadeobservable.common.api.model.User;
import me.mitkovic.android.handmadeobservable.common.view.Presenter;

public interface MainPresenter extends Presenter<MainView> {

    void onClickUser(Context context, User user);

}
