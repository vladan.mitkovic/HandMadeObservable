package me.mitkovic.android.handmadeobservable.home.view;

import android.content.Context;
import android.os.Bundle;

import java.util.List;

import javax.inject.Inject;

import hugo.weaving.DebugLog;
import icepick.Icepick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.mitkovic.android.handmadeobservable.R;
import me.mitkovic.android.handmadeobservable.common.api.UsersAPI;
import me.mitkovic.android.handmadeobservable.common.api.model.User;


public class MainPresenterImpl implements MainPresenter {

    private UsersAPI usersAPI;

    private MainView view;

    private CompositeDisposable subscription;

    @Inject
    public MainPresenterImpl(UsersAPI usersAPI) {
        this.usersAPI = usersAPI;
    }

    @Override
    public void onBind(MainView view) {
        this.view = view;
        setLoading(true);

        subscription = new CompositeDisposable();

        getData();
    }

    private void getData() {
        Disposable resultSubscription = usersAPI.getUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successConsumer(), errorConsumer());

        subscription.add(resultSubscription);
    }

    private Consumer<List<User>> successConsumer() {
        return new Consumer<List<User>>() {

            @Override
            @DebugLog
            public void accept(final List<User> users) throws Exception {
                setLoading(false);
                view.setSuccessSnackBar();
                view.setAdapterData(users);
            }
        };
    }

    private Consumer<Throwable> errorConsumer() {
        return new Consumer<Throwable>() {

            @Override
            @DebugLog
            public void accept(Throwable throwable) throws Exception {
                setLoading(false);
                view.setErrorMessage(throwable.getMessage());
            }
        };
    }

    @Override
    public void onClickUser(Context context, User user) {
        view.setUserClickSnackBar(context.getString(R.string.user_message) + user.getFirstName());
    }

    private void setLoading(boolean loading) {
        view.setLoading(loading);
    }

    @Override
    public void onUnbind() {
        this.view = null;

        if (subscription != null) {
            subscription.dispose();
            subscription = null;
        }
    }

    @Override
    public void onRestore(Bundle bundle) {
        Icepick.restoreInstanceState(this, bundle);
    }

    @Override
    public void onSave(Bundle bundle) {
        Icepick.saveInstanceState(this, bundle);
    }

}
