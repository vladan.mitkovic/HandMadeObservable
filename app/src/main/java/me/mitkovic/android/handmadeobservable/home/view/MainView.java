package me.mitkovic.android.handmadeobservable.home.view;


import java.util.List;

import me.mitkovic.android.handmadeobservable.common.api.model.User;
import me.mitkovic.android.handmadeobservable.common.view.View;

public interface MainView extends View {

    void setAdapterData(List<User> users);

    void setSuccessSnackBar();

    void setErrorMessage(String errorMessage);

    void setLoading(boolean isLoading);

    void setUserClickSnackBar(String string);

}
