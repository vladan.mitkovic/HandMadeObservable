package me.mitkovic.android.handmadeobservable.home.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.mitkovic.android.handmadeobservable.R;
import me.mitkovic.android.handmadeobservable.common.api.model.User;

public class UserRow extends FrameLayout {

    @BindView(R.id.user_name)
    TextView userName;

    @BindView(R.id.user_avatar)
    SimpleDraweeView userAvatar;

    private User user;

    public interface Listener {
        void onClickUser(User user);
    }

    public UserRow(final Context context) {
        super(context);
        initialize(context);
    }

    public UserRow(final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    private void initialize(final Context context) {
        inflate(context, R.layout.list_item_user, this);

        setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        ButterKnife.bind(this, this);
    }

    public void setListener(final Listener listener) {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClickUser(user);
            }
        });
    }

    public void setUser(final User user) {
        this.user = user;

        userName.setText(user.getFirstName());
        userAvatar.setImageURI(user.getProfilePictureSmall());
    }

}
