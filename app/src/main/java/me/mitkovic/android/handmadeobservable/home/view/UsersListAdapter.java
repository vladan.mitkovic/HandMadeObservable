package me.mitkovic.android.handmadeobservable.home.view;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import me.mitkovic.android.handmadeobservable.common.api.model.User;

public class UsersListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private UserRow.Listener listener;
    private List<User> users = new ArrayList<>();

    @Inject
    public UsersListAdapter() {
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserViewHolder(new UserRow(parent.getContext()), listener);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserViewHolder) {
            ((UserViewHolder) holder).bind(users.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void setUsers(List<User> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    public void setListener(UserRow.Listener listener) {
        this.listener = listener;
    }

}
